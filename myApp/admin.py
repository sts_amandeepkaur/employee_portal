from django.contrib import admin
from myApp.models import myIp,attendance,employee,emp_details,HR,document,leave,holiday,Notification,leaveAssign,leaveApply,announcement,event,HrNotification,attendance_user,comments,contactus

class emp_detailsAdmin(admin.ModelAdmin):
    list_display=('id','title','emp_name','date_of_birth','email','a_phone','address','city','state','country','pin_code')
class attendanceAdmin(admin.ModelAdmin):
    list_display=('id','check_in_time','check_out_time')    
class employeeAdmin(admin.ModelAdmin):
    list_display=('country','state','city','contact','image')   
class attendance_userAdmin(admin.ModelAdmin):
    list_display=('id','date','time', 'attendance_type')
class leaveApplyAdmin(admin.ModelAdmin):
    list_display=('leave_type','emp_id','from_date','to_date','for_days','status','session','description','sub_date')
class leaveAssignAdmin(admin.ModelAdmin):
    list_display=('ltype','total','date','added_on')
class documentAdmin(admin.ModelAdmin):
    list_display=('doc','document','reference','description','submission_date','status')
class NotificationAdmin(admin.ModelAdmin):
    list_display=('user_id','status','content','notify_on')
class HrNotificationAdmin(admin.ModelAdmin):
    list_display=('notify_type','status','content','notify_on')
class announcementAdmin(admin.ModelAdmin):
    list_display=('hr_announce','status','notify_on','text')
class eventAdmin(admin.ModelAdmin):
    list_display=('hr_event','status','notify_on','event_text')
class commentsAdmin(admin.ModelAdmin):
    list_display=('text','name','on_date')
class holidayAdmin(admin.ModelAdmin):
    list_display=('hr_holiday','date','day','holidays','status','present_date')
class HRAdmin(admin.ModelAdmin):
    list_display=('user','gender','country','state','city','contact','image') 
class contactusAdmin(admin.ModelAdmin):
    list_display=('name','email','subject','message')
      
admin.site.register(employee,employeeAdmin)
admin.site.register(emp_details,emp_detailsAdmin)
admin.site.register(HR,HRAdmin)
admin.site.register(document,documentAdmin)
admin.site.register(leave)
admin.site.register(Notification,NotificationAdmin)
admin.site.register(leaveAssign,leaveAssignAdmin)
admin.site.register(leaveApply,leaveApplyAdmin)
admin.site.register(announcement,announcementAdmin)
admin.site.register(holiday,holidayAdmin)
admin.site.register(attendance,attendanceAdmin)
admin.site.register(event,eventAdmin)
admin.site.register(HrNotification,HrNotificationAdmin)
admin.site.register(comments,commentsAdmin)
admin.site.register(attendance_user, attendance_userAdmin)
admin.site.register(contactus, contactusAdmin)
admin.site.register(myIp)



